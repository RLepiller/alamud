from .action import Action2
from mud.events import MdpOnEvent

class MdpOnAction(Action2):
    EVENT = MdpOnEvent
    ACTION = "mdp-on"
    RESOLVE_OBJECT = "resolve_for_use"

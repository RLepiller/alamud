# -*- coding: utf-8 -*-
# Copyright (C) 2014 Denys Duchier, IUT d'Orléans
#==============================================================================

from .action import Action1
from mud.events import DanseEvent

class DanseAction(Action1):
    EVENT = DanseEvent
    ACTION = "danse"

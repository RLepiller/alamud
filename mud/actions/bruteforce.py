from .action import Action2
from mud.events import BruteEvent

class BruteAction(Action2):
    EVENT = BruteEvent
    ACTION = "bruteforce"
    RESOLVE_OBJECT="resolve_for_operate"

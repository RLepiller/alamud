# -*- coding: utf-8 -*-
# Copyright (C) 2014 Denys Duchier, IUT d'Orléans
#==============================================================================

from .action import Action3
from mud.events import TypeEvent

class TypeAction(Action3):
    EVENT = TypeEvent
    ACTION = "type"
    RESOLVE_OBJECT = "resolve_for_operate"

    def __init__(self, subject, object):
        if object != "01010101011011100010000001110000011001010111010001101001011101000010000000110010001100000010111100110010001100000010000000111111":
            super().__init__(subject, "ordinateur", object)
        else:
            super().__init__(subject,"portable",object)
            
    def resolve_object2(self):
        return self.object2

from .event import Event1

class MapEvent(Event1):
    def perform(self):
        self.buffer_clear()
        self.buffer_append("Vous êtes perdu!")
        self.buffer_append("<img class='map' src='/static/img/map.png'/>")
        self.actor.send_result(self.buffer_get())
